import { Stack, StackProps } from 'aws-cdk-lib';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as rds from 'aws-cdk-lib/aws-rds';
import { Construct } from 'constructs';
import { createMysqlInstance } from '../module/rds';
import { dbScheduleLambda } from '../module/lambda';

type DatabaseStackProps = StackProps & {
  vpc: ec2.Vpc
};

export class DatabaseStack extends Stack {
  constructor(scope: Construct, id: string, props?: DatabaseStackProps) {
    super(scope, id, props);

    const env = scope.node.tryGetContext("env");

    if (props) {
      const mysql = createMysqlInstance(
        this,
        env["serviceName"],
        env["stage"],
        props.vpc,
        ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO),
        rds.MysqlEngineVersion.VER_8_0
      );

      if (env["stage"] != 'prd') {
        // 指定した時間帯だけ開始停止するLambdaを定義する
        dbScheduleLambda(
          this,
          env["serviceName"],
          env["stage"],
          mysql.instanceArn,
          '23:45', // UTC(JSTなら08:45)
          '09:00' // UTC(JSTなら18:00)
        );
      }
    }
  }
}
