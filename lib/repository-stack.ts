import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { createCodeCommit, createGitlabIamUser } from '../module/codecommit';
import * as codecommit from "aws-cdk-lib/aws-codecommit";

export class RepositoryStack extends Stack {
  public readonly codecommit: codecommit.Repository; // ✅ 別スタックで読み込めるようにする

  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const env = scope.node.tryGetContext("env");

    // リポジトリの生成およびGitLabとの連携ポリシー作成
    const codecommit = createCodeCommit(
      this,
      env["serviceName"],
      `repogitory for ${env["serviceName"]}`
    );

    if (env["gitlabMirror"] === true) {
      createGitlabIamUser(
        this,
        env["serviceName"],
        codecommit
      );
    }

    this.codecommit = codecommit;
  }
}
