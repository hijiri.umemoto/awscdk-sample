import { Stack, StackProps } from 'aws-cdk-lib';
import { Vpc } from "aws-cdk-lib/aws-ec2";
import { Construct } from 'constructs';
import { createVpc } from '../module/vpc'

export class VpcStack extends Stack {
  public readonly vpc: Vpc; // ✅ 別スタックで読み込めるようにする

  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const env = scope.node.tryGetContext("env");

    const vpc = createVpc(
      this,
      env["serviceName"],
      env["stage"]
    );

    this.vpc = vpc;
  }
}
