# AWS環境構築

AWS CDKによる開発環境構築

本サンプルはVSCodeのDevcontainerを活用し、AWS CDKの実行環境を構築します。

## GitLab⇒CodeCommitスタック

```bash
cdk bootstrap
cdk deploy RepositoryStack
aws iam create-service-specific-credential --user-name ${サービス名}-gitlab-mirroring-user --service-name codecommit.amazonaws.com
# gitの認証情報が表示されます。後で利用するためメモ必須
```

## Useful commands

* `yarn build`
    * compile typescript to js
* `yarn watch`
    * watch for changes and compile
* `yarn test`
    * perform the jest unit tests
* `cdk deploy`
    * deploy this stack to your default AWS account/region
* `cdk diff`
    * compare deployed stack with current state
* `cdk synth`
    * emits the synthesized CloudFormation template
