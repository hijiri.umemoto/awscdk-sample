import * as codecommit from "aws-cdk-lib/aws-codecommit";
import * as iam from "aws-cdk-lib/aws-iam";
import { Construct } from "constructs";

export const createCodeCommit = (
  app: Construct,
  serviceName: string,
  description: string
): codecommit.Repository => {
  return new codecommit.Repository(
    app,
    `${serviceName}-codecommit`,
    {
      repositoryName: serviceName,
      description: description
    }
  );
};

export const createGitlabIamUser = (
  app: Construct,
  serviceName: string,
  codecommit: codecommit.Repository
): iam.User => {
  const policy_statement = new iam.PolicyStatement({
    effect: iam.Effect.ALLOW,
    actions: [
      "codecommit:GitPull",
      "codecommit:GitPush"
    ],
    resources: [
      codecommit.repositoryArn
    ]
  });

  const policy = new iam.Policy(
    app,
    `${serviceName}-gitlab-mirroring-policy`,
    {
      statements: [policy_statement],
      policyName: `${serviceName}-gitlab-mirroring-policy`
    }
  );

  const user = new iam.User(
    app,
    `${serviceName}-gitlab-mirroring-user`,
    {
      userName: `${serviceName}-gitlab-mirroring-user`
    }
  );

  user.attachInlinePolicy(policy);

  return user;
};
