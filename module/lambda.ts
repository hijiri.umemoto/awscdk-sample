import * as path from 'path';
import { Duration } from 'aws-cdk-lib';
import * as events from 'aws-cdk-lib/aws-events';
import * as eventsTargets from 'aws-cdk-lib/aws-events-targets';
import * as logs from 'aws-cdk-lib/aws-logs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as nodejsLambda from 'aws-cdk-lib/aws-lambda-nodejs';
import * as iam from 'aws-cdk-lib/aws-iam';
import { Construct } from "constructs";

export const dbScheduleLambda = (
  app: Construct,
  serviceName: string,
  stage: string,
  targetDbArn: string,
  startTime: string,
  endTime: string,
  onlyWeekDay: boolean = true
) => {
  // Lambda関数の定義
  const startLambda = new nodejsLambda.NodejsFunction(
    app,
    `${serviceName}-${stage}-rds-start`,
    {
      functionName: `${serviceName}-${stage}-rds-start`,
      runtime: lambda.Runtime.NODEJS_LATEST,
      timeout: Duration.minutes(5),
      memorySize: 1024,
      handler: 'handler',
      entry: path.join(__dirname, '../lambda/rds-schduler/index.ts'),
      environment: {
        DB_ARN: targetDbArn,
        MODE: "start"
      },
      logRetention: logs.RetentionDays.ONE_MONTH,
      bundling: {
        banner: "import { createRequire } from 'module';const require = createRequire(import.meta.url);",
        externalModules: ['@aws-sdk/*'],
        minify: true,
        tsconfig: path.join(__dirname, '../tsconfig.json'),
        format: nodejsLambda.OutputFormat.ESM
      }
    }
  );
  const stopLambda = new nodejsLambda.NodejsFunction(
    app,
    `${serviceName}-${stage}-rds-end`,
    {
      functionName: `${serviceName}-${stage}-rds-end`,
      runtime: lambda.Runtime.NODEJS_LATEST,
      timeout: Duration.minutes(5),
      memorySize: 1024,
      handler: 'handler',
      entry: path.join(__dirname, '../lambda/rds-schduler/index.ts'),
      environment: {
        DB_ARN: targetDbArn,
        MODE: "stop"
      },
      logRetention: logs.RetentionDays.ONE_MONTH,
      bundling: {
        banner: "import { createRequire } from 'module';const require = createRequire(import.meta.url);",
        externalModules: ['@aws-sdk/*'],
        minify: true,
        tsconfig: path.join(__dirname, '../tsconfig.json'),
        format: nodejsLambda.OutputFormat.ESM
      }
    }
  );

  // 権限付与
  const describePolicy = new iam.PolicyStatement({
    effect: iam.Effect.ALLOW,
    actions: [
      "rds:DescribeDBInstances",
      "rds:DescribeDBClusters"
    ],
    resources: ['*']
  });
  const schedulePolicy = new iam.PolicyStatement({
    effect: iam.Effect.ALLOW,
    actions: [
      "rds:StartDBInstance",
      "rds:StartDBCluster",
      "rds:StopDBInstance",
      "rds:StopDBCluster"
    ],
    resources: [
      targetDbArn
    ]
  });
  startLambda.addToRolePolicy(describePolicy);
  startLambda.addToRolePolicy(schedulePolicy);
  stopLambda.addToRolePolicy(describePolicy);
  stopLambda.addToRolePolicy(schedulePolicy);

  // スケジュール登録
  const [startHour, startMinute] = startTime.split(':');
  const startEvent = new events.Rule(
    app,
    `${serviceName}-${stage}-rds-start-event`,
    {
      ruleName: `${serviceName}-${stage}-rds-start-event`,
      schedule: events.Schedule.cron({
        hour: startHour,
        minute: startMinute,
        weekDay: onlyWeekDay ? 'MON-FRI' : 'SUN-SAT'
      }),
      targets: [
        new eventsTargets.LambdaFunction(startLambda, { retryAttempts: 3 })
      ]
    }
  );
  const [endHour, endMinute] = endTime.split(':');
  const endEvent = new events.Rule(
    app,
    `${serviceName}-${stage}-rds-end-event`,
    {
      ruleName: `${serviceName}-${stage}-rds-end-event`,
      schedule: events.Schedule.cron({
        hour: endHour,
        minute: endMinute,
        weekDay: onlyWeekDay ? 'MON-FRI' : 'SUN-SAT'
      }),
      targets: [
        new eventsTargets.LambdaFunction(stopLambda, { retryAttempts: 3 })
      ]
    }
  );
};