import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as rds from 'aws-cdk-lib/aws-rds';
import { IInstanceEngine } from 'aws-cdk-lib/aws-rds';
import { Vpc, SubnetSelection, ISubnet } from "aws-cdk-lib/aws-ec2";
import { Construct } from "constructs";

export const createMysqlInstance = (
  app: Construct,
  serviceName: string,
  stage: string,
  vpc: Vpc,
  instanceType: ec2.InstanceType = ec2.InstanceType.of(ec2.InstanceClass.T3, ec2.InstanceSize.MEDIUM),
  engineVersion: rds.MysqlEngineVersion = rds.MysqlEngineVersion.VER_8_0
) => {
  // 基本ISOlATEのサブネット
  const subnets: ISubnet[] = vpc.isolatedSubnets;
  const allAll = ec2.Port.allTraffic();
  const tcp3306 = ec2.Port.tcpRange(3306, 3306);

  const dbsg = new ec2.SecurityGroup(
    app,
    `${serviceName}-${stage}-db-sg`,
    {
      vpc: vpc,
      allowAllOutbound: false,
      description: `${serviceName}-${stage}-Database`,
      securityGroupName: `${serviceName}-${stage}-db-sg`
    }
  );
  dbsg.addIngressRule(dbsg, tcp3306, 'all from self');

  const engine: IInstanceEngine = rds.DatabaseInstanceEngine.mysql({
    version: engineVersion
  });

  const database: rds.DatabaseInstance = new rds.DatabaseInstance(
    app,
    `${serviceName}-${stage}-db`,
    {
      vpc: vpc,
      vpcSubnets: { subnets },
      databaseName: serviceName.replace('-', ''),
      instanceIdentifier: `${serviceName}-${stage}-db`,
      engine: engine,
      instanceType: instanceType,
      allowMajorVersionUpgrade: true,
      caCertificate: rds.CaCertificate.RDS_CA_2019
    }
  );

  return database;
};
