import { Vpc, SubnetConfiguration, SubnetType } from "aws-cdk-lib/aws-ec2";
import { Construct } from "constructs";

export const createVpc = (
  app: Construct,
  serviceName: string,
  stage: string,
  cidr: string = "10.0.0.0/16",
  natGateways: number = 0,
  maxAzs: number = 2
) => {
  // PublicSubnetとPrivateSubnet, IsolatedSubnetをAZ毎に一つずつ作成する
  let publicSubnet: SubnetConfiguration = {
    name: 'public',
    subnetType: SubnetType.PUBLIC,
    cidrMask: 24
  };
  let privateSubnet: SubnetConfiguration = {
    name: 'private',
    subnetType: SubnetType.PRIVATE_WITH_EGRESS,
    cidrMask: 24
  };
  let isolatedSubnet: SubnetConfiguration = {
    name: 'isolated',
    subnetType: SubnetType.PRIVATE_ISOLATED,
    cidrMask: 24
  };

  return new Vpc(
    app,
    `${serviceName}-vpc`,
    {
      vpcName: `${serviceName}-${stage}-vpc`,
      cidr: cidr,
      natGateways: natGateways,
      maxAzs: maxAzs,
      subnetConfiguration: [
        publicSubnet,
        privateSubnet,
        isolatedSubnet
      ]
    }
  );
};
