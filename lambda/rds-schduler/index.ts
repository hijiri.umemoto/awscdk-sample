import { ScheduledEvent, Context } from 'aws-lambda';
import * as AWS from 'aws-sdk';


const getDBInstance = async (
  rds: AWS.RDS,
  targetArn: string
): Promise<AWS.RDS.DBInstance | null> => {
  // インスタンスの一覧取得
  const instances = await rds.describeDBInstances().promise();

  for (const db of (instances.DBInstances ?? [])) {
    if (targetArn == db.DBInstanceArn) {
      return db;
    }
  }

  return null;
};


export const handler = async (
  event: ScheduledEvent,
  context: Context
) => {
  const rds = new AWS.RDS();
  const mode: string = process.env['MODE'] ?? '';

  const db = await getDBInstance(
    rds,
    process.env['DB_ARN'] ?? ''
  );

  if (db) {
    if (mode == 'start') {
      // 開始処理
      const resp = await rds.startDBInstance({
        DBInstanceIdentifier: db.DBInstanceIdentifier ?? ''
      }).promise();
    } else if (mode == 'stop') {
      // 停止処理
      const resp = await rds.stopDBInstance({
        DBInstanceIdentifier: db.DBInstanceIdentifier ?? ''
      }).promise();
    }
  }
};
