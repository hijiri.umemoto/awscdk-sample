#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import { VpcStack } from '../lib/vpc-stack';
import { RepositoryStack } from '../lib/repository-stack';
import { DatabaseStack } from '../lib/database-stack';

const app = new cdk.App();

const repository = new RepositoryStack(app, 'RepositoryStack');

const vpc = new VpcStack(app, 'VpcStack');
const database = new DatabaseStack(app, 'DatabaseStack', {
  vpc: vpc.vpc
});